FROM ubuntu:20.04
LABEL name="spo"
COPY main-3.c .
COPY Makefile .
COPY start.sh .
RUN apt update
RUN apt install -y --allow-unauthenticated \
    gcc \
    clang \
    cmake \
    make
RUN apt-get update && apt-get install -y wget  \
    lsb-release  \
    software-properties-common  \
    gnupg  \
    make
RUN wget https://apt.llvm.org/llvm.sh && chmod u+x llvm.sh && ./llvm.sh 19
RUN apt-get install -y     libomp-19-dev