no-omp:
	clang-19 -O3 -Ofast -ffinite-math-only -freciprocal-math -frounding-math -Wall -Werror -pedantic -o lab3-no-omp main-3.c -lm

omp:
	clang-19 -O3 -Ofast -ffinite-math-only -freciprocal-math -frounding-math -Wall -Werror=implicit-function-declaration -pedantic -o lab3-omp main-3.c -lm -fopenmp -lomp

build-lab: no-omp omp