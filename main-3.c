#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
//#include <pthread.h>
#include <unistd.h>

void foo(int i, int N, float *mess_1, float *mess_2);

#if defined(_OPENMP)
#include <omp.h>
#endif

//Параметры варианта
//A = 280
//map = 4/6
//merge = 4
//sort = 4
//N1 = 474 - 10мс
//N2 = 11543 - 5000 мс
//
// Стандартный файл
//𝑁 = 𝑁1, 𝑁1 + Δ, 𝑁1 + 2Δ, 𝑁1 + 3Δ, . . . , 𝑁2
//
//Для скриптов с кол ядер
//𝑁 = 𝑁1, 𝑁1 + Δ, 𝑁1 + 2Δ, 𝑁1 + 3Δ, . . . , 𝑁2
//
//
//значение Δ выбрать так: Δ = (𝑁2 − 𝑁1)/10 -> (11543-474)/10 = 1107
//
//
//Провести верификацию значения X. Добавить в конец цикла вывод значения X и изменить число экспериментов на 5. Сравнить значения X
//для распараллеленной программы и нераспараллеленной.
#define DELAY 1
int progress = 0;

//
#ifdef _OPENMP
void print_progress() {
    int num_of_experements = 100;
    while (progress < num_of_experements) {
        sleep(DELAY);
        float percentage = (float) progress / (float) num_of_experements * 100.0;
        printf("%.0f%%\n", percentage);
    }
}
#endif

float random_float(float min, float max) {
#ifdef _OPENMP
    int seed;
    return min + ((float) rand_r(&seed) / (float) RAND_MAX) * (max - min);
#else
    return min + ((float) rand() / (float) RAND_MAX) * (max - min);
#endif
}

void generateMess_1(float *mess, int n) {
#ifdef _OPENMP
    #if defined(CHUNK_SIZE) && defined(SCHED_ALG)
#pragma omp parallel for default(none) shared(mess, n) schedule(SCHED_ALG, CHUNK_SIZE)
#else
#pragma omp parallel for default(none) shared(mess, n) schedule(auto)
#endif
#endif
    for (int i = 0; i < n; i++) {
        mess[i] = random_float(1.0, 280.0);
    }
}

void generateMess_2(float *mess, int n) {
#ifdef _OPENMP
    #if defined(CHUNK_SIZE) && defined(SCHED_ALG)
#pragma omp parallel for default(none) shared(mess, n) schedule(SCHED_ALG, CHUNK_SIZE)
#else
#pragma omp parallel for default(none) shared(mess, n) schedule(auto)
#endif
#endif
    for (int i = 0; i < n; i++) {
        mess[i] = random_float(280.0, 280.0 * 10.0);
    }
}

void hyperbolic_cotangent_of_sqrt_mess(float *mess, int n) {
#ifdef _OPENMP
    #if defined(CHUNK_SIZE) && defined(SCHED_ALG)
#pragma omp parallel for default(none) shared(mess, n) schedule(SCHED_ALG, CHUNK_SIZE)
#else
#pragma omp parallel for default(none) shared(mess, n) schedule(auto)
#endif
#endif
    for (int i = 0; i < n; i++) {
        mess[i] = 1.0 / tanh(sqrt(mess[i]));
    }
}

void sum__mess(float *mess, int n) {
    float oldElement = 0;
#ifdef _OPENMP
    #if defined(CHUNK_SIZE) && defined(SCHED_ALG)
#pragma omp parallel for default(none) shared(mess, n, oldElement) schedule(SCHED_ALG, CHUNK_SIZE)
#else
#pragma omp parallel for default(none) shared(mess, n, oldElement) schedule(auto)
#endif
#endif
    for (int i = 0; i < n; i++) {
        float sum = mess[i] + oldElement;
        oldElement = mess[i];
        mess[i] = exp(log10(sum));
    }
}

void marge(float *mess1, float *mess2, int size) {
#ifdef _OPENMP
    #if defined(CHUNK_SIZE) && defined(SCHED_ALG)
#pragma omp parallel for default(none) shared(mess1, mess2, size) schedule(SCHED_ALG, CHUNK_SIZE)
#else
#pragma omp parallel for default(none) shared(mess1, mess2, size) schedule(auto)
#endif
#endif
    for (int i = 0; i < size; i++) {
        mess2[i] = (mess1[i] > mess2[i]) ? mess1[i] : mess2[i];
    }
}

void gnome_sort(float arr[], int size) {
    int index = 0;
    while (index < size) {
        if (index == 0 || arr[index] >= arr[index - 1]) {
            index++;
        } else {
            float temp = arr[index];
            arr[index] = arr[index - 1];
            arr[index - 1] = temp;
            index--;
        }
    }
}

#ifdef _OPENMP
void parallel_sort(float arr[], int size) {
    if(size%2 != 0){
        size += 1;
    }
    int size1 = size/2 , size2 = size/2;
    float *left = (float *) malloc(size1 * sizeof(float));
    float *right = (float *) malloc(size2 * sizeof(float));
    int j = 0;
    for (int i = 0; i < size1; i++) {
        left[j] = arr[i];
        j++;
    }
    j = 0;
    for (int i = size2; i < size; i++) {
        left[j] = arr[i];
        j++;
    }
    for (int i = 0; i < size; i++) {
        arr[i] = 0;
    }
#pragma omp parallel sections
    {
#pragma omp section
            gnome_sort(left, size1);
#pragma omp section
            gnome_sort(right, size2);
    }
    j = 0;
    int i = 0, k = 0;

    while(i < size1 && j < size2) {
        if(left[i] <= right[j]){
            arr[k++] = left[i++];
        } else {
             arr[k++] = right[j++];
        }
    }

    while(i < size1){
        arr[k++] = left[i++];
    }

    while(j < size2){
        arr[k++] = right[j++];
    }
}
#endif

void print(float *mess, int n) {
    printf("[");
#ifdef _OPENMP
    #if defined(CHUNK_SIZE) && defined(SCHED_ALG)
#pragma omp parallel for default(none) shared(mess, n) schedule(SCHED_ALG, CHUNK_SIZE)
#else
#pragma omp parallel for default(none) shared(mess, n) schedule(auto)
#endif
#endif
    for (int i = 0; i < n - 1; i++) {
        printf("%f, ", mess[i]);
    }
    printf("%f", mess[n - 1]);
    printf("]\n");
}

float sum_of_sines(float *M2, int size) {
    // Находим минимальный ненулевой элемент массива M2
    int min_nonzero = -1;
    float sum = 0;
#ifdef _OPENMP
    #if defined(CHUNK_SIZE) && defined(SCHED_ALG)
#pragma omp parallel for default(none) shared(M2, size, min_nonzero, sum) schedule(SCHED_ALG, CHUNK_SIZE)
#else
#pragma omp parallel for default(none) shared(M2, size, min_nonzero, sum) schedule(auto)
#endif
#endif
    for (int i = 0; i < size; ++i) {
        if (M2[i] != 0 && (min_nonzero == -1 || M2[i] < M2[min_nonzero])) {
            min_nonzero = i;
        }
    }
    // Просматриваем элементы массива M2
    for (int i = 0; i < size; ++i) {
        // Проверяем, делится ли элемент на минимальный ненулевой элемент на четное число
        if ((int) M2[i] % (int) M2[min_nonzero] == 0) {
            if (((int) (M2[i] / M2[min_nonzero])) % 2 == 0) {
                // Если да, добавляем синус элемента к сумме
                sum += sin(M2[i]);
            }
        }
    }

    return sum;
}

void main_work(int N) {
    int i;
    float *mess_1;
    float *mess_2;
#ifdef _OPENMP
    double start_time, end_time;
        start_time = omp_get_wtime();
#else
    struct timeval T1, T2;
    gettimeofday(&T1, NULL);
#endif
    mess_1 = (float *) malloc(N * sizeof(float));
    mess_2 = (float *) malloc(N / 2 * sizeof(float));
    int num_of_experements = 100;
    for (i = 0; i < num_of_experements; i++) { // 100 экспериментов
        srand(i); // инициализировать начальное значение ГСЧ
        generateMess_1(mess_1, N);
        generateMess_2(mess_2, N / 2);
        hyperbolic_cotangent_of_sqrt_mess(mess_1, N);
        sum__mess(mess_2, N / 2);
        marge(mess_1, mess_2, N / 2);
#ifdef _OPENMP
        parallel_sort(mess_2, N / 2);
#else
        gnome_sort(mess_2, N / 2);
#endif
        sum_of_sines(mess_2, N / 2);
        progress++;
        //printf("i - %d => x = %f\n", i, x);
    }
#ifdef _OPENMP
    end_time = omp_get_wtime();
        printf("%f\n", (end_time - start_time) * 1000);
#else
    gettimeofday(&T2, NULL);
    printf("%ld\n", 1000 * (T2.tv_sec - T1.tv_sec) + (T2.tv_usec - T1.tv_usec) / 1000);
#endif
}

int main(int argc, char *argv[]) {
    int N = atoi(argv[1]);
#ifdef _OPENMP
    omp_set_nested(1);
    #pragma omp parallel sections shared(N, progress) num_threads(2)
    {
        #pragma omp section
            {
                main_work(N);
            }
        #pragma omp section
            {
                print_progress();
            }
    }
#else
    main_work(N);
#endif
    return 0;
}
