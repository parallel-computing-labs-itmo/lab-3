#!/bin/bash

echo 'no-omp:' > out.txt
for ((i = 470; i <= 12400; i += 1193)); do
    ./lab3-no-omp $i >> out.txt
done
echo '' >> out.txt
echo 'omp:' >> out.txt
for ((i = 470; i <= 12400; i += 1193)); do
    ./lab3-omp $i >> out.txt
done
